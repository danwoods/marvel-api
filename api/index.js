/**
 * @fileoverview Wrapper to make working with Marvel's API easier
 * @example http://localhost:3000/75960620002300111
 * @example http://localhost:3000/series/31388
 */

const crypto = require('crypto')
const https = require('https')

// Load env vars if not in now env
// (now doesn't load `.env.example`)
if (!process.env.NOW_REGION) {
  require('dotenv-safe').config()
}

/**
 * Given a URL, create a URL to request from Marvel's API
 * @param {string} url - Requesting URL
 * @param {string} apiKey - API Key
 * @param {string} hash - MD5 auth hash
 * @param {string} timestamp - Timestamp for auth
 * @see https://developer.marvel.com/documentation/authorization
 */
const createUrl = (url, apiKey, hash, timestamp) => {
  try {
    let newUrl = new URL('https://gateway.marvel.com:443/v1/public/')
    const reqPath = url.replace('/', '')
    const newSearch = new URLSearchParams(
      reqPath.includes('?') ? reqPath.substring(reqPath.indexOf('?') + 1) : ''
    )

    // If this is in the form of /upc (default), set that up
    if (!isNaN(+reqPath)) {
      newUrl.pathname = newUrl.pathname + 'comics'
      newSearch.append('upc', reqPath)
    }
    // Otherwise, just pass the end of the URL along
    else {
      newUrl.pathname = newUrl.pathname + reqPath
    }

    // Add authentication
    newSearch.append('apikey', apiKey)
    newSearch.append('hash', hash)
    newSearch.append('ts', timestamp)

    return `${newUrl.toString()}?${newSearch.toString()}`
  } catch (err) {
    console.error(err)
    return ''
  }
}

/**
 * Create hash and timestamp required for talking to Marvel's API
 * @param {string} pub - Public API key
 * @param {string} prv - Private API key
 * @return {[string, string]} - [Timestamp, Hash]
 */
const createTimestampAndHash = (pub, prv) => {
  const ts = new Date().getTime()
  const hash = crypto
    .createHash('md5')
    .update(ts + prv + pub)
    .digest('hex')

  return [ts, hash]
}

/**
 * Accept request with upc barcode and return matching issue
 * @param {object} req - HTTP request
 * @return {object} - Response from Marvel's API
 */
module.exports = async (req, res) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'OPTIONS, GET',
    'Access-Control-Max-Age': 2592000, // 30 days
    'Content-Type': 'application/json'
  }

  // Set response headers
  Object.entries(headers).forEach(([key, val]) => res.setHeader(key, val))

  // If OPTIONS call, no path, or call for favicon, return 204
  if (
    req.method === 'OPTIONS' ||
    req.url === '/' ||
    req.url === '/favicon.ico'
  ) {
    res.status = 204
    res.end()
  }
  // Else, call Marvel's API
  else {
    /** Marvel API public key */
    const PUBLIC_KEY = process.env.MARVEL_PUBLIC_KEY
    /** Marvel API private key */
    const PRIVATE_KEY = process.env.MARVEL_PRIVATE_KEY
    /** Timestamp and hash to authenticate with Marvel's API */
    const [ts, hash] = createTimestampAndHash(PUBLIC_KEY, PRIVATE_KEY)
    /** URL to hit Marvel's API */
    const url = createUrl(req.url, PUBLIC_KEY, hash, ts)
    /** Response from Marvel's API */
    const marvelResp = await new Promise((resolve, reject) =>
      https.get(url, (res) => {
        /** Response body from Marvel's API */
        let body = ''

        // Specify response encoding
        res.setEncoding('utf8')

        // Concat as data comes in
        res.on('data', (data) => {
          body += data
        })

        // On end of data, convert `body` to Object and resolve
        res.on('end', () => {
          body = JSON.parse(body)
          resolve(body)
        })
      })
    )

    // If response good, return data
    if (marvelResp && marvelResp.data && marvelResp.data.results) {
      res.end(JSON.stringify(marvelResp.data.results))
    }
    // Else return full response
    else {
      res.end(JSON.stringify(marvelResp))
    }

    return
  }
}
