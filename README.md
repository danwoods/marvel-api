# marvel-api

marvel-api is a simple faas wrapper around [Marvel's API](https://developer.marvel.com/docs). Currently only supports looking up by UPC barcode.

## Usage

```bash
MARVEL_PRIVATE_KEY=ABC MARVEL_SECRET_KEY=XYZ npx gitlab:danwoods/marvel-api
```

_then, from another terminal_
```bash
curl http://localhost:3000/75960609261100911 | json_pp
```


